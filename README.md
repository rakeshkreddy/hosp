# Installation Steps:

Recommendation Installation Steps:
1. Install NodeJS and NPM first.
2. Then Navigate to node-api-mysql folder.
3. Run 'npm install'
4. Now Navigate to src folder
5. Run 'npm install'

If the above steps do not work, install the dependencies listed below.

# NodeJS
1. Download and Install NodeJS from https://nodejs.org/en/ (Please see NPM Section too. It has a combined package available)
2. After installation, verify by running node -v in terminal / command prompt

# NPM
1. To Install NPM cum NodeJS: https://www.npmjs.com/get-npm
2. To check if you have Node.js installed, run this command in your terminal: node -v
3. To confirm that you have npm installed you can run this command in your terminal: npm -v

# Angular
1. To Install : npm install -g @angular/cli

# Express
1. To Install Express: npm install express --save

# CORS
1. To Install CORS: npm install cors

# MySQL
1. To Install MySQL: npm i mysql

# Configure the Database
1. To login to the Database use : mysql -u root -p where root is the username/admin. By default there is no password.
2. Create user 'test' : 
    a) CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';
    b) GRANT ALL PRIVILEGES ON *.* TO 'test'@'localhost';
3. Login as test user: mysql -r test -p , Hit Enter, on the prompt type 'test' and hit enter 
4. Create database : CREATE DATABASE hospital;
5. Use Database : use hospital;
6. Create Schema: 
    CREATE TABLE `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `age` INT NOT NULL,
    `gender` TEXT NOT NULL,
    `nationality` TEXT NOT NULL,
    `pdfFile` LONGBLOB,
    PRIMARY KEY (`id`)
    );



# Steps to Run the Application
1. Go to  node-api-mysql folder and run node index.js
2. In another terminal or command prompt, go to src folder and run ng serve --open. This will open the Login Page. Create a user and you can start doing the search.

# MyHospital

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
