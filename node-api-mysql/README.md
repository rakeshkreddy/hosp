
# Install Express
npm i express

# Install MySQL
npm i mysql
brew install mysql

# To fix Issue like below:
	# Unable to connect to host 127.0.0.1, or the request timed out.
	# Be sure that the address is correct and that you have the necessary privileges, or try increasing the connection timeout (currently 10 seconds).
	# MySQL said: Authentication plugin 'caching_sha2_password' cannot be loaded: dlopen(/usr/local/lib/plugin/caching_sha2_password.so, 2)

	# Fix:
	1. Go to my.cnf file and in section [mysqld] add line: (my.cnf location: /usr/local/etc/my.cnf )

		default-authentication-plugin=mysql_native_password

	2. Login to mysql server from terminal: run mysql -u root -p, then inside shell execute this command (replacing [password] with your actual password):

		ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '[password]';

	3. exit from mysql shell with exit and run brew services restart mysql.

# Useful MySQL Tips

1. Check the MySQL Server Status: mysql.server status
2. Restart MySQL Server: mysql.server restart
3. Restart MySQL via BREW: brew services restart mysql
4. Login as ROOT: mysql -u root -p
5. Login as User: mysql -u <user> -p

