
const fs = require("fs");
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'test',
  password : 'test',
  database : 'hospital'
});

const getUserById = (request, response) => {
  const id = parseInt(request.params.id)
  
  //const loc = request.params.loc
  //%2Fusers
  //console.log(loc)
  //sleep(5000);
  connection.query('SELECT id, name, age, gender, nationality FROM users WHERE id = ?', [id], function(err, rows, fields) {
    if (!err && rows.length > 0) {
      //console.log('The solution is: ', rows);
      response.status(200).json(rows);

      // Check to read it from DB:
  
      /*connection.query('select pdfFile from users WHERE id = ?', [id], function(err, res) {
        if (err) 
          throw err;
          
        const row = res[0];

        // Got BLOB data:
        const data = row.pdfFile;
        //console.log(data);

        var oFile = id + "_op.pdf"
        //console.log(oFile);

        // Converted to Buffer:
        const buf = new Buffer(data, "binary");
        //console.log(bug);

        // Write new file out:
        fs.writeFileSync(oFile, buf);
        
      });*/
    }
    else {
      response.status(201).json(rows);
      //console.log('Error while performing Query.');
    }
  });

  
}

function sleep( millisecondsToWait )
{
  var now = new Date().getTime();
  while ( new Date().getTime() < now + millisecondsToWait );
}

const createUser = (request, response) => {
  const { id, name, age, gender, nationality, pdfFile } = request.body
  
  //console.log(request);
  // Read buffer of PDF file:
  console.log(pdfFile);

  sleep(10000);

  const data = readImageFile(pdfFile); // `data`'s type is Buffer
  
  //console.log(data);

  connection.query('INSERT INTO users (id, name, age, gender, nationality, pdfFile) VALUES (?, ?, ?, ?, ?, ?)', 
    [id, name, age, gender, nationality, data], function(err, rows, fields) {
  
    if (err) {
      throw err
    }

    response.status(201).send(`User added with ID: ${id}`)
  })
}

function readImageFile(file) {
  // read binary data from a file:
  const bitmap = fs.readFileSync(file);
  const buf = new Buffer(bitmap);
  return buf;
}

module.exports = {
  getUserById,
  createUser
}


