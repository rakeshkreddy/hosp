const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./hospitalqueries')
const port = 3000

const cors = require('cors')
app.options('*', cors());

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}

app.use(bodyParser.json())
app.use(cors());
app.use(allowCrossDomain);
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

//app.get('/auth', db.getAuth)
//app.get('/users/:id&:loc', db.getUserById)
app.get('/patient/:id', db.getUserById)
app.post('/patient', db.createUser)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})
