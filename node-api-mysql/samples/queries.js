const fs = require("fs");
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'test',
  password : 'test',
  database : 'hospital'
});


//connection.connect();

const getTestTable = (request, response) => {
  connection.query('SELECT * from tablename', function(err, rows, fields) {
    if (!err) {
      //console.log('The solution is: ', rows);
      response.status(200).json(rows);
    }
    else
      console.log('Error while performing Query.');
  });
}


const putTestTable = (request, response) => {
const inputfile = "/Users/rkosana/Desktop/test.pdf";
const outputfile = "/Users/rkosana/Desktop/retest.pdf";

// Read buffer of an image file:
const data = readImageFile(inputfile); // `data`'s type is Buffer
console.log("About to insert BLOB data !");
console.log(data);
var insert_R = 'INSERT INTO bindata(data) VALUE(?)';
connection.query('SELECT * from tablename', function(err, rows, fields) {});
connection.query(insert_R, [data], function(err, res) {
  if (err) 
    throw err;
  
  console.log("BLOB data inserted!");
  
  // Check to read it from DB:
  
  connection.query("select * from `bindata`", function(err, res) {
  if (err) 
    throw err;

  console.log(res[0]);
  const row = res[0];
  // Got BLOB data:
  const data = row.data;
  console.log("BLOB data read!");
  // Converted to Buffer:
  const buf = new Buffer(data, "binary");
  // Write new file out:
  fs.writeFileSync(outputfile, buf);
  console.log("New file output:", outputfile);
  });
});
response.status(200).json();
}

function readImageFile(file) {
// read binary data from a file:
const bitmap = fs.readFileSync(file);
const buf = new Buffer(bitmap);
return buf;
}

//connection.end();

module.exports = {
  getTestTable,
  putTestTable
}


