var mysql = require('mysql');
var fs = require("fs");
 
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'test',
    password: 'test',
    database: 'hospital',
    debug: false,
});
connection.connect();
 
var dog = {
    img: fs.readFileSync("/Users/rkosana/MyProjects/myHospital/node-api-mysql/samples/data.png"),
    file_name: 'Dog'
};
 
var cat = {
    img: fs.readFileSync("data.png"),
    file_name: 'Cat'
};
 
connection.query('INSERT INTO trn_image SET ?', dog, function(err, rows, fields) {
    if (!err)
    	console.log(rows);
    else
	console.log("Erro");
});

connection.query('SELECT * from tablename', function(err, rows, fields) {
    if (!err) {
      console.log('The solution is: ', rows);
      //response.status(200).json(rows);
    }
    else
      console.log('Error while performing Query.');
  });
 
query = connection.query('INSERT INTO trn_image SET ?', cat, function(err,
    result) {
    if (!err)
    	console.log(result);
    else
	console.log("Erro");
});
 
connection.end();
