import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataAccessService {  
  
  // Base URL - Host where RESTful Service will be available
  baseUrl = 'http://localhost:3000';
  currentImageSource: any;
  img = new Image();
  constructor(private httpClient : HttpClient) {}

  setImageSource(arg0: string) {
    this.currentImageSource = arg0;
    var dataURL;
    var src = "file:///Users//rkosana//Desktop//sign.jpeg";
    
    this.img.src = src;
    /*image.onload = function() {
        dataURL = getBase64Image(image);
    };*/
}

  /*getBase64Image() {
    
    
    console.log(this.currentImageSource);
    var canvas = document.createElement("canvas");
    console.log("image");
    canvas.width = this.currentImageSource.width;
    canvas.height = this.currentImageSource.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(this.currentImageSource, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
  }*/


  public getBase64Image() {
    var canvas = document.createElement("canvas");
    canvas.width = this.img.width;
    canvas.height = this.img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(this.img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }
    

  
  // Add Methods that would talk to RESTFul Service

  // Fetch Kases required to Populate Menu
  /*getKases() {
    return this.httpClient.get(`${this.baseUrl}/kases`);
  }*/

  getUserById (index: Number) {
    const url = `${this.baseUrl}/patient/${index}`; 
    return this.httpClient.get(url, httpOptions);
  }

  createUser(user: { id: string; name: string; age: string; gender: string; nationality: string; pdfFile: string; }) {
    return  this.httpClient.post(`${this.baseUrl}/patient/`, user);
  }
  

  // End of Methods that would talk to RESTFul Service
}
